"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Product = void 0;
var user_entity_1 = require("src/auth/user.entity");
var typeorm_1 = require("typeorm");
var typeorm_2 = require("typeorm");
var Product = /** @class */ (function (_super) {
    __extends(Product, _super);
    function Product() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        typeorm_2.PrimaryGeneratedColumn('uuid')
    ], Product.prototype, "id");
    __decorate([
        typeorm_2.Column()
    ], Product.prototype, "product_img");
    __decorate([
        typeorm_2.Column()
    ], Product.prototype, "product_name");
    __decorate([
        typeorm_2.Column()
    ], Product.prototype, "product_description");
    __decorate([
        typeorm_2.Column()
    ], Product.prototype, "product_price");
    __decorate([
        typeorm_1.ManyToOne(function (type) { return user_entity_1.User; }, function (user) { return user.product; }, { eager: false }),
        typeorm_1.JoinColumn()
    ], Product.prototype, "user");
    __decorate([
        typeorm_2.Column()
    ], Product.prototype, "userId");
    Product = __decorate([
        typeorm_2.Entity()
    ], Product);
    return Product;
}(typeorm_2.BaseEntity));
exports.Product = Product;
